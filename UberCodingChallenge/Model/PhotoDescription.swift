//
//  Photo.swift
//  UberCodingChallenge
//
//  Created by Michal Kalinowski on 29/12/2017.
//  Copyright © 2017 Michal Kalinowski. All rights reserved.
//

import Foundation

struct PhotoDescription: Decodable {
  let farm: Int
  let id: String
  let secret: String
  let server: String
  
  // only decode what we'll need
  private enum CodingKeys: String, CodingKey {
    case farm, id, secret, server
  }
}
