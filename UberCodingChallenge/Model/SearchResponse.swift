//
//  SearchResponse.swift
//  UberCodingChallenge
//
//  Created by Michal Kalinowski on 29/12/2017.
//  Copyright © 2017 Michal Kalinowski. All rights reserved.
//

import Foundation

struct SearchResponse: Decodable {
  static let OKStatus = "ok"
  
  let result: SearchResult
  let status: String
  
  private enum CodingKeys: String, CodingKey {
    case result="photos", status = "stat"
  }
}

