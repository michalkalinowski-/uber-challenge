//
//  SearchResult.swift
//  UberCodingChallenge
//
//  Created by Michal Kalinowski on 29/12/2017.
//  Copyright © 2017 Michal Kalinowski. All rights reserved.
//

import Foundation

struct SearchResult: Decodable {
  let page: Int
  let numberOfPages: Int
  let photosPerPage: Int
  let photos: [PhotoDescription]
  
  private enum CodingKeys: String, CodingKey {
    case page, numberOfPages="pages", photosPerPage="perpage", photos="photo"
  }
}
