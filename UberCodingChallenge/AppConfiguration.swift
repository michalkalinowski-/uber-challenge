//
//  AppConfiguration.swift
//  UberCodingChallenge
//
//  Created by Michal Kalinowski on 29/12/2017.
//  Copyright © 2017 Michal Kalinowski. All rights reserved.
//

import Foundation

/// Extract secrets and other stuff that should not be commited
/// to public repo here and handle accordingly
struct AppConfiguration {
  // Secrets
  static let flickrAPIKey = "3e7cc266ae2b0e0d78e279ce8e361736"
  
  // UI Configuration
  static var resultsPerPage = 50
}


