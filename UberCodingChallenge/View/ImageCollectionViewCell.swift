//
//  ImageCollectionViewCell.swift
//  UberCodingChallenge
//
//  Created by Michal Kalinowski on 28/12/2017.
//  Copyright © 2017 Michal Kalinowski. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell, ReuseIdentifiable {
  
  var imageProvider: ImageDataProvider? {
    didSet {
      imageProvider?.getImage {
        imageData in
        if let data = imageData {
            self.imageView.image = UIImage(data: data)
        }
      }
    }
  }
  
  lazy private var imageView: UIImageView = {
    let imageView = UIImageView()
    imageView.translatesAutoresizingMaskIntoConstraints = false
    imageView.contentMode = .scaleAspectFit
    imageView.clipsToBounds = true
    
    self.addSubview(imageView)
    return imageView
  }()
  
  override func prepareForReuse() {
    super.prepareForReuse()
    imageView.image = nil
    // Very Important: need to dealocate the provider when cell gets reused
    imageProvider = nil
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    backgroundColor = .white
    imageView.frame = self.bounds
  }
  
}
