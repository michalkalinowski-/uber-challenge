//
//  ImageProvider.swift
//  UberCodingChallenge
//
//  Created by Michal Kalinowski on 30/12/2017.
//  Copyright © 2017 Michal Kalinowski. All rights reserved.
//

import Foundation

class ImageDataProvider {
  // TODO: inject as dependency rather than make it public
  lazy var httpRequest = HTTPRequest()
  private var photoDescription: PhotoDescription
  
  init(photoDescription: PhotoDescription) {
    self.photoDescription = photoDescription
  }
  
  deinit {
    httpRequest.task?.cancel()
  }
  
  func getImage(callback: @escaping(Data?)->Void) {
    let getImageRequest = Request.getImage(farm: photoDescription.farm,
                                           server: photoDescription.server,
                                           id: photoDescription.id,
                                           secret: photoDescription.secret)
    httpRequest.request(getImageRequest) {
      data, error in
      // TODO: handle errors (or at least log them)
      // passing data straight through for now
      DispatchQueue.main.async {
        callback(data)
      }
    }
  }
}
