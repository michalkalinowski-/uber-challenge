//
//  ViewController.swift
//  UberCodingChallenge
//
//  Created by Michal Kalinowski on 28/12/2017.
//  Copyright © 2017 Michal Kalinowski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  private lazy var searchResultsDataSource = SearchResultsDataSource()
  
  private lazy var searchResultsCollectionView: UICollectionView = {
    let flowLayout = UICollectionViewFlowLayout()
    flowLayout.itemSize = CGSize(width: self.view.bounds.width/3 - 8,
                                 height: self.view.bounds.width/3 - 8)
    
    // initialazing frame to zero since we use auto layout
    let collectionView = UICollectionView(frame: CGRect.zero,
                                          collectionViewLayout: flowLayout)
    // register cells
    collectionView.register(ImageCollectionViewCell.self,
                            forCellWithReuseIdentifier: ImageCollectionViewCell.reuseIdentifier)
    
    // set up delegate and data source
    collectionView.delegate = self
    collectionView.dataSource = self.searchResultsDataSource
    
    // UI
    collectionView.translatesAutoresizingMaskIntoConstraints = false
    collectionView.backgroundColor = UIColor.appBackgroundColor
    
    self.view.addSubview(collectionView)
    return collectionView
  }()
  
  private lazy var searchBar: UISearchBar = {
    let searchBar = UISearchBar()
    searchBar.translatesAutoresizingMaskIntoConstraints = false
    searchBar.placeholder = "What images would you like to see?"
    searchBar.delegate = self
    
    self.view.addSubview(searchBar)
    return searchBar
  }()

  override func viewDidLoad() {
    super.viewDidLoad()
    
    // match background color to UICollectionView bc otherwise it will show on iPX
    view.backgroundColor = UIColor.appBackgroundColor
    layoutViews()
  }

  private func layoutViews() {
    // search bar layout
    searchBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
    searchBar.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    searchBar.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
    
    // collection view layout
    searchResultsCollectionView.topAnchor.constraint(equalTo: searchBar.bottomAnchor).isActive = true
    searchResultsCollectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    searchResultsCollectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    searchResultsCollectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
  }
}

extension ViewController: UISearchBarDelegate {
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    // naive input validation
    guard let query = searchBar.text, !query.isEmpty else {
      return
    }
    
    print("search query: \(query)")
    searchResultsDataSource.loadResultsForQuery(query) {
      // need a hard reload here because contents of collection view has changed
      self.searchResultsCollectionView.reloadData()
      self.searchResultsCollectionView.setContentOffset(CGPoint.zero, animated: false)
    }
    searchBar.endEditing(false)
  }
}

extension ViewController: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView,
                      willDisplay cell: UICollectionViewCell,
                      forItemAt indexPath: IndexPath) {
    // load more properties when getting close to the bottom
    if indexPath.row == collectionView.numberOfItems(inSection: indexPath.section) - AppConfiguration.resultsPerPage/2 {
      searchResultsDataSource.loadMoreResults() {
        newIndices in
        guard newIndices.count > 0 else { return }
        self.searchResultsCollectionView.insertItems(at: newIndices)
      }
    }
  }
}
