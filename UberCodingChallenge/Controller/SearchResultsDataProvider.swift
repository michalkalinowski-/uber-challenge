//
//  SearchResultsDataProvider.swift
//  UberCodingChallenge
//
//  Created by Michal Kalinowski on 29/12/2017.
//  Copyright © 2017 Michal Kalinowski. All rights reserved.
//

import UIKit

class SearchResultsDataProvider {
  private var query: String
  private var nextPage = 1
  private var lastPage = Int.max
  
  var searchResults = [PhotoDescription]()
  var numberOfResults: Int {
    return searchResults.count
  }
  
  // TODO: inject as dependency rather than make it public
  lazy var httpRequest = HTTPRequest()
  
  init(forQuery query: String) {
    self.query = query
  }
  
  deinit {
    // if there is an ongoing search we want to cancel it
    // when we get killed
    httpRequest.task?.cancel()
  }
  
  private func decodeSearchResponse(_ data: Data) -> SearchResponse? {
    let decoder = JSONDecoder()
    return try? decoder.decode(SearchResponse.self, from: data)
  }
  
  func loadMoreResults(callback: @escaping (_ numberOfNewResults: Int)->Void) {
    
    guard nextPage <= lastPage else {
      // we have reached the last page of the search results
      callback(0)
      return
    }
    
    let searchRequest = Request.search(query: query, page: nextPage)
    httpRequest.request(searchRequest) {
      data, error in
      // TODO: handle errors and display relevant info to the user
      guard let data = data else {
        callback(0)
        return
      }
      
      // TODO: handle errors here as well
      guard let searchResponse = self.decodeSearchResponse(data),
        searchResponse.status == SearchResponse.OKStatus else {
        callback(0)
        return
      }
      
      // update page
      self.nextPage = searchResponse.result.page + 1
      self.searchResults += searchResponse.result.photos
      self.lastPage = searchResponse.result.numberOfPages - 1
      
      // always call UI stuff on main thread
      DispatchQueue.main.async {
        callback(searchResponse.result.photos.count)
      }
    }
  }
}
