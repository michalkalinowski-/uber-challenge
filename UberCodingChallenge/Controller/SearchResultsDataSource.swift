//
//  SearchResultsDataSource.swift
//  UberCodingChallenge
//
//  Created by Michal Kalinowski on 28/12/2017.
//  Copyright © 2017 Michal Kalinowski. All rights reserved.
//

import UIKit

class SearchResultsDataSource: NSObject {
  
  private var dataProvider: SearchResultsDataProvider?
  
  // API here
  func loadResultsForQuery(_ query: String, callback: @escaping ()->Void) {
    // create new data source for given query
    dataProvider = SearchResultsDataProvider(forQuery: query)
    // call back when done
    dataProvider!.loadMoreResults() {
      _ in
      callback()
    }
  }
  
  func loadMoreResults(callback: @escaping (_ newIndices: [IndexPath])->Void) {
    guard let dataProvider = self.dataProvider else {
      fatalError("Trying to load more results when no query was specified")
    }
    
    dataProvider.loadMoreResults() {
      newResultsCount in
      callback(self.numberOPfNewResultsToIndexPaths(newResultsCount))
    }
  }
  // END API
  
  private func numberOPfNewResultsToIndexPaths(_ newResultsCount: Int) -> [IndexPath] {
    guard let dataProvider = dataProvider else {
      // TODO: log error to analytics to indicate faulty app state
      return []
    }
    
    let newRowRange = (dataProvider.numberOfResults-newResultsCount)..<dataProvider.numberOfResults
    return newRowRange.map {IndexPath(row: $0, section: 0)}
  }
}

extension SearchResultsDataSource: UICollectionViewDataSource {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return dataProvider?.numberOfResults ?? 0
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    guard let dataProvider = dataProvider else {
      // TODO: log inconsistent app state
      fatalError()
    }
    
    let photoDescription = dataProvider.searchResults[indexPath.row]
    let imageProvider = ImageDataProvider(photoDescription: photoDescription)
    
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.reuseIdentifier, for: indexPath) as! ImageCollectionViewCell
    cell.imageProvider = imageProvider
    return cell
  }
}
