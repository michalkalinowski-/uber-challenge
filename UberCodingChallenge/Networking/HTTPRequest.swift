//
//  NetworkHandler.swift
//  UberCodingChallenge
//
//  Created by Michal Kalinowski on 29/12/2017.
//  Copyright © 2017 Michal Kalinowski. All rights reserved.
//

import Foundation

class HTTPRequest {
  // MARK: API
  func request(_ request: Request, callback: @escaping(Data?, Error?) -> Void) {
    if let task = self.task {
      // network request ongoing, need to cancel it
      task.cancel()
    }
    
    task = session.dataTask(with: request.url) {
      data, response, error in
      // clean up after you're done
      defer { self.task = nil }
      
      guard error == nil else {
        // TODO: cast error into app specific enum
        // instead of passing a raw error from network layer
        callback(nil, error)
        return
      }
      
      guard let response = response as? HTTPURLResponse,
        response.statusCode == 200,
        let data = data else {
          // TODO: use app specific enum for errors and pass to callback
          callback(nil, nil)
          return
      }
      
      // response is ok callback with data
      callback(data, nil)
      }
    task?.resume()
  }
  
  // MARK: Private
  lazy private var sessionConfiguration: URLSessionConfiguration = {
    let configuration = URLSessionConfiguration.default
    // add custom session configuration here
    return configuration
  }()
  
  lazy private var session = URLSession(configuration: sessionConfiguration)
  
  // (hacky but fast) let's expose this one to be able to cancel it
  // from outside of this class and also test this behaviour
  var task: URLSessionDataTask?
}
