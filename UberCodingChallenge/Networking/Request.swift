//
//  Request.swift
//  UberCodingChallenge
//
//  Created by Michal Kalinowski on 29/12/2017.
//  Copyright © 2017 Michal Kalinowski. All rights reserved.
//

import Foundation

enum Request {
  case search(query: String, page: Int)
  case getImage(farm: Int, server: String, id: String, secret: String)
  
  private var rootURL: String {
    switch self {
    case .search:
      return "https://api.flickr.com/services/rest/"
    case .getImage(let farm, let server, let id, let secret):
      return "https://farm\(farm).static.flickr.com/" +
             "\(server)/" +
             "\(id)_\(secret)_t.jpg"
    }
  }
  
  private var query: String {
    switch self {
    case .search(let query, let page):
      return "method=flickr.photos.search" +
            "&api_key=\(AppConfiguration.flickrAPIKey)" +
            "&format=json&nojsoncallback=1" +
            "&safe_search=1" +
            "&text=\(query)" +
            "&per_page=\(AppConfiguration.resultsPerPage)" +
            "&page=\(page)"
    case .getImage:
      return ""
    }
  }
  
  private var urlComponents: URLComponents {
    // In production app where URLs are assigned from other files based on test/prod env
    // more graceful error handling would be required here
    guard var components = URLComponents(string: rootURL) else {
      fatalError("API URL is invalid")
    }
    components.query = query
    return components
  }
  
  var url: URL {
    guard let url = urlComponents.url else {
      fatalError("Couldn't build valid urls from request parameters, check app configuration")
    }
    return url
  }
}
