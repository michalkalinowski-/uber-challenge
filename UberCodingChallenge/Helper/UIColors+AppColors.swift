//
//  UIColors+appColors.swift
//  UberCodingChallenge
//
//  Created by Michal Kalinowski on 28/12/2017.
//  Copyright © 2017 Michal Kalinowski. All rights reserved.
//

import UIKit

extension UIColor {
  static var appBackgroundColor = UIColor(white: 0.91, alpha: 1)
}
