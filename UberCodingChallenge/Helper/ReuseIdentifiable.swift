//
//  ReuseIdentifiable.swift
//  UberCodingChallenge
//
//  Created by Michal Kalinowski on 28/12/2017.
//  Copyright © 2017 Michal Kalinowski. All rights reserved.
//

import Foundation

import Foundation

protocol ReuseIdentifiable: class {
  static var reuseIdentifier: String { get }
}

extension ReuseIdentifiable {
  static var reuseIdentifier: String {
    return String(describing: self)
  }
}
