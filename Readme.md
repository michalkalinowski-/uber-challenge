Uber Challenge
---

time spent: ~8h

## Shortcuts taken:
Given the time constraints and requirement to implement the project without using any frameworks I had to take **many** shortcuts. I've tried to mark them all with `// TODO:` tag. Most notable ones include:

* [Networking stack] There's pretty much no error handling. No network, response errors etc are not reported in any way to the user.
* [Flickr API] I do not parse or try to handle/log errors returned by Flickr API
* [Project] No logging is done (I usually use a framework to do multi-level logging)
* [Project] No crash reporting is done

## Tests
Xcode reports code coverage at around 50%. Although proper way to test most of the classes would be to inject dependencies that are hiden behind a protocol, I had to take some shortcuts due to time constraints.

## Performance
Performance of the app is OK. Memory usage is between 60-100Mb depending on the speed of scrolling. There are no memory leaks. No frame dropping all the way down to iPhone 6

Further perfomance improvements can be made by:
* implementing image cache
* implementing UICollectionViewDataSourcePrefetching to download images in advance and put them in cache




