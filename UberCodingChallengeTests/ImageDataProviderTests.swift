//
//  ImageDataProviderTests.swift
//  UberCodingChallengeTests
//
//  Created by Michal Kalinowski on 30/12/2017.
//  Copyright © 2017 Michal Kalinowski. All rights reserved.
//

import XCTest
@testable import UberCodingChallenge

class ImageDataProviderTests: XCTestCase {
    
    func testImageRequestIsBuiltProperly() {
      // setup
      let photoDescription = PhotoDescription(farm: 666666, id: "id6", secret: "secret6", server: "server6")
      let imageProvider = ImageDataProvider(photoDescription: photoDescription)
      let requestMock = HTTPRequestMock()
      imageProvider.httpRequest = requestMock
      
      // test
      imageProvider.getImage(callback: {_ in })
      
      // assert
      let requestSubmitted = requestMock.requestSubmitted!
      let urlString = requestSubmitted.url.absoluteString
      
      XCTAssert(urlString.contains("666666"), "Farm not passed correctly")
      XCTAssert(urlString.contains("id6"), "ID not passed correctly")
      XCTAssert(urlString.contains("secret6"), "Secret not passed correctly")
      XCTAssert(urlString.contains("server6"), "Server not passed correclty")
    }
}
