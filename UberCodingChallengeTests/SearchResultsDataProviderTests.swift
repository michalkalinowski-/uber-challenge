//
//  SearchResultsDataProviderTests.swift
//  UberCodingChallengeTests
//
//  Created by Michal Kalinowski on 30/12/2017.
//  Copyright © 2017 Michal Kalinowski. All rights reserved.
//

import XCTest
@testable import UberCodingChallenge

class SearchResultsDataProviderTests: XCTestCase {
  
  // When data provider is asked to load more results it should increment
  // page number in the request url
  func testLoadMoreIncrementsPage() {
    // setup
    let httpRequestMock = HTTPRequestMock()
    httpRequestMock.networkResponse = getSearchResponseStub(forPage: 1)
    let searchResultsProvider = SearchResultsDataProvider(forQuery: "123")
    searchResultsProvider.httpRequest = httpRequestMock
    
    // test
    searchResultsProvider.loadMoreResults(callback: {_ in})
    searchResultsProvider.loadMoreResults(callback: {_ in})
    // page requested should be 2 at this point
    
    // assert
    let requestSubmitted = httpRequestMock.requestSubmitted
    XCTAssert(requestSubmitted?.url.absoluteString.contains("&page=2") == true)
  }
  
  // when data provider is asked to load more results for the first time
  // it should ask for page 1 of the results
  func testLoadMoreAsksForPageOneFirstTimeItsCalled() {
    // setup
    let httpRequestMock = HTTPRequestMock()
    let searchResultsProvider = SearchResultsDataProvider(forQuery: "123")
    searchResultsProvider.httpRequest = httpRequestMock
    
    // test
    searchResultsProvider.loadMoreResults(callback: {_ in})
    // assert
    let requestSubmitted = httpRequestMock.requestSubmitted
    XCTAssert(requestSubmitted?.url.absoluteString.contains("&page=1") == true)
  }
  
  // callback of loadMoreResults needs to receive count of photos received
  // this tests checks if the count is returned and if it's correct
  func testLoadMoreRetursCorrectNewResultsCount() {
    // setup
    let httpRequestMock = HTTPRequestMock()
    httpRequestMock.networkResponse = getSearchResponseStub(forPage: 1)
    let searchResultsProvider = SearchResultsDataProvider(forQuery: "123")
    searchResultsProvider.httpRequest = httpRequestMock
    
    let expectation = XCTestExpectation(description: "Results count is returned")
    
    // test
    searchResultsProvider.loadMoreResults() {
      newResultsCount in
      print(newResultsCount)
      // 3 is the count of array in stub below
      XCTAssert(newResultsCount == 3)
      expectation.fulfill()
    }
    
    wait(for: [expectation], timeout: 10)
  }
}

extension SearchResultsDataProviderTests {
  private func getSearchResponseStub(forPage page: Int) -> Data {
    let responseString = """
    {\"photos\":{\"page\":\(page),\"pages\":666,\"perpage\":10,\"total\":\"211416\",\"photo\":[{\"id\":\"25501525168\",\"owner\":\"37082505@N06\",\"secret\":\"ce6d29545b\",\"server\":\"4734\",\"farm\":5,\"title\":\"Villamartin22\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0},{\"id\":\"38492337525\",\"owner\":\"150411396@N07\",\"secret\":\"fb0eeb11f7\",\"server\":\"4683\",\"farm\":5,\"title\":\"IMG_8324\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0},{\"id\":\"24505412627\",\"owner\":\"150411396@N07\",\"secret\":\"6655939294\",\"server\":\"4730\",\"farm\":5,\"title\":\"IMG_8518\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}]},\"stat\":\"ok\"}
    """
    return responseString.data(using: .utf8)!
  }
}
