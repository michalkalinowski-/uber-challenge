//
//  FlickrAPIModelTests.swift
//  UberCodingChallengeTests
//
//  Created by Michal Kalinowski on 29/12/2017.
//  Copyright © 2017 Michal Kalinowski. All rights reserved.
//

import XCTest
@testable import UberCodingChallenge

class FlickrAPIModelTests: XCTestCase {
  
  lazy var searchResponse: SearchResponse = {
    let decoder = JSONDecoder()
    return try! decoder.decode(SearchResponse.self, from: self.responseStub)
  }()
    
  
  func testSearchResponsePopulatesStatus() {
    XCTAssert(searchResponse.status == "ok")
  }
  
  func testSearchResultPopulatesAllFields() {
    // this is mostly to check if there are no bugs in key mapping
    // no point testing swift decoders
    let searchResult = searchResponse.result
    XCTAssert(searchResult.numberOfPages == 21142)
    XCTAssert(searchResult.page == 1)
    XCTAssert(searchResult.photosPerPage == 10)
    XCTAssert(!searchResult.photos.isEmpty)
  }
  
  func testSearchResultParsesAllThePhotos() {
    let photos = searchResponse.result.photos
    XCTAssert(photos.count == searchResponse.result.photosPerPage)
  }
  
  func testPhotoPopulatesAllTheFields() {
    // this is mostly to check if there are no bugs in key mapping
    // no point testing swift decoders
    guard let photo = searchResponse.result.photos.first else {
      XCTFail("Photo parsing doesn't work - check testSearchResultParsesAllThePhotos test")
      return
    }
    
    XCTAssert(photo.farm == 5)
    XCTAssert(photo.id == "25501525168")
    XCTAssert(photo.secret == "ce6d29545b")
    XCTAssert(photo.server == "4734")
  }
}

extension FlickrAPIModelTests {
  private var responseString: String {
    return """
    {\"photos\":{\"page\":1,\"pages\":21142,\"perpage\":10,\"total\":\"211416\",\"photo\":[{\"id\":\"25501525168\",\"owner\":\"37082505@N06\",\"secret\":\"ce6d29545b\",\"server\":\"4734\",\"farm\":5,\"title\":\"Villamartin22\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0},{\"id\":\"38492337525\",\"owner\":\"150411396@N07\",\"secret\":\"fb0eeb11f7\",\"server\":\"4683\",\"farm\":5,\"title\":\"IMG_8324\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0},{\"id\":\"24505412627\",\"owner\":\"150411396@N07\",\"secret\":\"6655939294\",\"server\":\"4730\",\"farm\":5,\"title\":\"IMG_8518\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0},{\"id\":\"39370469471\",\"owner\":\"149721825@N08\",\"secret\":\"3ee3a47542\",\"server\":\"4690\",\"farm\":5,\"title\":\"2017-12-29_12-59-24\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0},{\"id\":\"39340483252\",\"owner\":\"150411396@N07\",\"secret\":\"acc141e6c9\",\"server\":\"4731\",\"farm\":5,\"title\":\"IMG_1766\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0},{\"id\":\"27593160459\",\"owner\":\"150411396@N07\",\"secret\":\"faf43909d7\",\"server\":\"4592\",\"farm\":5,\"title\":\"IMG_3472\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0},{\"id\":\"38492370025\",\"owner\":\"150411396@N07\",\"secret\":\"613f9ae43f\",\"server\":\"4590\",\"farm\":5,\"title\":\"IMG_8137\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0},{\"id\":\"24505821337\",\"owner\":\"140466591@N06\",\"secret\":\"b3ef055dfa\",\"server\":\"4727\",\"farm\":5,\"title\":\"_MG_3676\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0},{\"id\":\"24505774457\",\"owner\":\"137531677@N07\",\"secret\":\"f31fb5f47b\",\"server\":\"4687\",\"farm\":5,\"title\":\"IMG_0248\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0},{\"id\":\"38661991374\",\"owner\":\"158998126@N05\",\"secret\":\"27b2771de6\",\"server\":\"4737\",\"farm\":5,\"title\":\"I see you\",\"ispublic\":1,\"isfriend\":0,\"isfamily\":0}]},\"stat\":\"ok\"}
    """
  }
  
  var responseStub: Data {
    return responseString.data(using: .utf8)!
  }
}
