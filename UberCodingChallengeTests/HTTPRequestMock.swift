//
//  HTTPRequestMock.swift
//  UberCodingChallengeTests
//
//  Created by Michal Kalinowski on 30/12/2017.
//  Copyright © 2017 Michal Kalinowski. All rights reserved.
//

import Foundation
@testable import UberCodingChallenge

class HTTPRequestMock: HTTPRequest {
  var requestSubmitted: Request?
  var networkResponse: Data?
  
  override func request(_ request: Request, callback: @escaping (Data?, Error?) -> Void) {
    self.requestSubmitted = request
    callback(networkResponse, nil)
  }
}
