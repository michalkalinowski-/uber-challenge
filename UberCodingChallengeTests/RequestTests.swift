//
//  RequestTests.swift
//  UberCodingChallengeTests
//
//  Created by Michal Kalinowski on 29/12/2017.
//  Copyright © 2017 Michal Kalinowski. All rights reserved.
//

import XCTest
@testable import UberCodingChallenge

class RequestTests: XCTestCase {
  
  func testSearchQueryIsURLEncoded() {
    let searchRequest = Request.search(query: "super hero", page: 1)
    XCTAssert(searchRequest.url.absoluteString.contains("super%20hero"), "Search query is not url encoded")
  }
  
  func testSearchRequestContainsCorrectAPIKey() {
    let searchRequest = Request.search(query: "", page: 1)
    XCTAssert(searchRequest.url.absoluteString.contains(AppConfiguration.flickrAPIKey), "Request doesn't contain API key specified in configuration")
  }
}
